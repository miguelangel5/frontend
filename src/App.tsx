import {useEffect, useState} from 'react'
import './App.css'
import {
    Box, Button, Dialog, DialogActions, DialogContent, DialogTitle,
    Grid,
    IconButton,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow, TextField
} from "@mui/material";
import axios from "axios";
import {Add, Camera, Delete, Edit, PhotoCamera} from "@mui/icons-material";

interface UserObject {
    id: number,
    name: string,
    lastName:string,
    email: string,
    password: string,
    image: string
}

function App() {
    const url = "http://localhost:3000/"

    const [open, setOpen] = useState(false);
    const [action, setAction] = useState("Save")
    const [users, setUsers] = useState<UserObject[]>([])
    const [name, setName] =useState("")
    const [lastName, setLastName] =useState("")
    const [email, setEmail] =useState("")
    const [id, setId] =useState(0)
    const [password, setPassword] =useState("")
    const [selectedImage, setSelectedImage] = useState<any>()
    const handleClose = () => {
        setOpen(false);
    };

    const onFileChange=(e:any)=> {
        let files = e.target.files;
        let fileReader = new FileReader();
        fileReader.readAsDataURL(files[0]);
        fileReader.onload = (event) => {
            // @ts-ignore
            setSelectedImage(event.target.result)
        }
    }

    const fetchUsers = async ()=>{
        axios.get(url+'api/usuarios')
            .then((response)=>{
                if (response.data.status){
                    setUsers(response.data.data)
                }
            })
            .catch(()=>{})
    }

    const deleteUser = async (id:number) =>{
        axios.delete(url+'api/usuarios/'+id)
            .then((response)=>{
                console.log(response)
                if (response.data.status){
                    fetchUsers();
                }
            })
    }

    const saveOrUpdateUser = async (action:String) =>{
        const body:UserObject ={
            id:0,
            name: name,
            lastName: lastName,
            email: email,
            password: password,
            image: selectedImage
        }
        setOpen(!open)
        if(action == "Save"){
            axios.post(url+'api/usuarios', body).then((response)=>{
                setOpen(!open)
                fetchUsers()
            })
                .catch((error=>{
                    console.log(error.response)
                    alert(error.response.data.error)
                }))
        }else{
            axios.put(url+'api/usuarios/'+id, body).then((response)=>{
                setOpen(!open)
                fetchUsers()
            })
                .catch((error=>{
                    console.log(error.response)
                    alert(error.response.data.error)
                }))
        }
        selectedImage(null)
    }

    useEffect(()=>{
        fetchUsers().then(()=>{})
    }, [])

  return (
    <div className="App">
        <Box mt={10} mx={10}>
            <Grid container>
                <Grid item xs={10}>
                    <h2>Current users</h2>
                </Grid>
                <Grid item xs={2}>
                    <Button onClick={()=>{
                        setOpen(!open)
                        setAction("Save")
                    }} fullWidth variant="contained" startIcon={<Add />}>
                        Add new user
                    </Button>
                </Grid>
            </Grid>
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell align="center">Picture</TableCell>
                            <TableCell align="center">Name</TableCell>
                            <TableCell align="center">Email</TableCell>
                            <TableCell align="center">Actions</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {users.map((row:UserObject, index:number) => (
                            <TableRow
                                key={index}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                                <TableCell align="center">
                                    <img src={`${row.image}`} />
                                </TableCell>
                                <TableCell align="center">{row.name} {row.lastName}</TableCell>
                                <TableCell align="center">{row.email}</TableCell>
                                <TableCell align="center">
                                    <IconButton color="primary" onClick={()=>{
                                        setOpen(!open)
                                        setAction("Edit")
                                        setId(row.id)}}>
                                        <Edit />
                                    </IconButton>
                                    <IconButton onClick={()=>{deleteUser(row.id)}} color="error">
                                        <Delete />
                                    </IconButton>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </Box>


        <Dialog
            open={open}
            onClose={handleClose}>
            <DialogTitle >
                {action} user
            </DialogTitle>
            <DialogContent>
                <Box>
                </Box>
                <Box mt={1} mb={1}>
                    <IconButton color="primary" aria-label="upload picture" component="label">
                        <input onChange={onFileChange} hidden accept="image/*" type="file" />
                        <PhotoCamera />
                    </IconButton>
                    <TextField fullWidth onChange={(e:any)=>setName(e.target.value)} label={"Name"}></TextField>
                    <TextField fullWidth onChange={(e:any)=>setLastName(e.target.value)} label={"Last Name"}></TextField>
                    <TextField fullWidth onChange={(e:any)=>setEmail(e.target.value)} label={"Email"}></TextField>
                    <TextField fullWidth onChange={(e:any)=>setPassword(e.target.value)} label={"Password"} type={"password"}></TextField>
                </Box>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose} variant={"outlined"}>Close</Button>
                <Button onClick={()=>{
                    saveOrUpdateUser(action)
                }} autoFocus variant={"contained"}>
                    {action}
                </Button>
            </DialogActions>
        </Dialog>


    </div>
  )
}

export default App
